use kaleidioscope_rust::parser;

fn main() {

    let tests = vec![
        "-23 + 24 * 5.66 + 5 + 5 - 4\0",
        "-0\0",
        "-0 / -0 + -0 * -0\0",
        "\"hello world\" + \"test one two three\"\0",
        "123 + \"test one two three\"\0",
    ];
    
    // let input = " [test, 1, 3, \"test\"]  \"test\" 24 * 5.66 + 5 + 5 - 4\0";

    // let input = "\"test\" 24 * 5.66 + 5 + 5 - 4\0";
    // let input = "-23 + 24 * 5.66 + 5 + 5 - 4\0";

    for t in tests {
        println!("");
        println!("Input: {0}", t);
        let mut parser = parser::Parser::new_with_input(String::from(t));
        
        let tokens = parser.parse();
    }

}
