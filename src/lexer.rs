#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    Eof,

    // commands
    Def,
    Extern,

    // primary
    Identifier(String),
    Number(f64),
    Other(char),
}

#[derive(Debug)]
pub enum LexErr {
    EndOfInputStream,
}

#[derive(Clone)]
pub struct Lexer {
    pub input: Vec<char>,
    pub identifier_str: String,
    pub num_val: f64,
    pub tokens: Vec<Token>,
}

impl Lexer {
    pub fn new() -> Lexer {
        Lexer {
            input: Vec::<char>::new(),
            identifier_str: String::new(),
            num_val: 0.0,
            tokens: Vec::<Token>::new(),
        }
    }

    pub fn new_with_input(input: String) -> Lexer {
        let split_input: Vec<char> = input.chars().collect();
        
        Lexer {
            input: split_input,
            identifier_str: String::new(),
            num_val: 0.0,
            tokens: Vec::<Token>::new(),
        }        
    }

    pub fn lex(&mut self) -> Vec<Token> {
        let mut tok = self.get_tok();
        let mut parsed = Vec::<Token>::new();
        parsed.push(tok.clone());
        println!("{:?}", tok);

        while tok != Token::Eof {
            tok = self.get_tok();
            parsed.push(tok.clone());
            
            println!("{:?}", tok);
        }
        
        self.tokens = parsed.clone();
        parsed
    }

    fn lex_identifier(&mut self, curr_char: char) -> Token {
        self.identifier_str = curr_char.to_string();
        
        let mut last_char = self.get_char();
        while last_char.is_alphanumeric() {
            self.identifier_str += &last_char.to_string()[..];
            last_char = self.get_char();
        }

        self.input.insert(0, last_char);

        if self.identifier_str == "def" {
            return Token::Def;
        }
        if self.identifier_str == "extern" {
            return Token::Extern;
        }
        return Token::Identifier(self.identifier_str.clone());
    }

    fn lex_number(&mut self, curr_char: char) -> Token {
        let mut num_str = curr_char.to_string();
        let mut last_char = self.get_char();
        let mut multiple_decimals = false;
        let mut seen_decimal = false;
        
        while last_char.is_numeric() || last_char == '.' {
            match last_char {
                '.' => {
                    if !seen_decimal { seen_decimal = true  } 
                    else { multiple_decimals = true }
                },
                _ => {}
            }

            num_str += &last_char.to_string()[..];

            last_char = self.get_char();
        }
        
        self.input.insert(0, last_char);

        if multiple_decimals { return Token::Identifier(num_str) }

        // TODO: Propagate this panic up, this will be unrecoverable but would be nice
        // to not handle it here and handle it in the Parser instead
        self.num_val = num_str.parse().expect(&format!(
            "Failed to convert num_str '{}' to an integer",
            &num_str
        ));

        Token::Number(self.num_val)
    }

    pub fn get_tok(&mut self) -> Token {
        let mut last_char = self.get_char();

        while last_char == ' ' {
            last_char = self.get_char();
        }
       
        if last_char.is_alphabetic() {
            return self.lex_identifier(last_char);
        }

        if last_char.is_numeric() {
            return self.lex_number(last_char);
        }

        if last_char == '#' {
            last_char = self.get_char();
            while last_char != '\n' || last_char != '\r' {
                last_char = self.get_char();
            }

            if last_char != '\0' {
                return self.get_tok();
            }
        }

        if last_char == '\0' {
            return Token::Eof;
        }

        return Token::Other(last_char);
    }

    pub fn get_char(&mut self) -> char {
        let result = match self.input.clone().split_first() {
            Some((result, rest)) => {
                self.input = rest.to_vec();
                Ok(*result)
            }
            _ => Err(LexErr::EndOfInputStream),
        };

        match result {
            Ok(next_char) => next_char,
            Err(_) => '\0',
        }
    }
}
