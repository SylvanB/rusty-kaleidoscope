use crate::lexer::{
    Lexer,
    Token,
};

pub type Identifier = String;

#[derive(Debug)]
pub enum Literal {
    FloatLit(f64),
    StringLit(String),
}

#[derive(Debug)]
pub enum OpCode {
    Addition,
    Subtraction,
    Multiplication,
    Division,
}

pub enum Stmt {
    Block(Vec<Box<Stmt>>, bool),
    If(Box<Expr>, Box<Stmt>, Box<Stmt>),
    While(Box<Expr>, Box<Stmt>),
    Return(Option<Box<Expr>>),
    Declaration(Identifier),
    FunDecl(Identifier, Vec<Identifier>, Box<Stmt>),
    Assignment(Box<Expr>, Box<Expr>),
    Struct(String, Vec<Identifier>),
}

#[derive(Debug)]
pub enum Expr {
    Id(Identifier),
    Literal(Literal),
    List(Vec<Box<Expr>>),
    BinaryOp(Box<Expr>, OpCode, Box<Expr>),
    UnaryOp(OpCode, Box<Expr>),
    FunCall(String, Vec<Expr>),
}

#[derive(Debug)]
pub enum ParseErr {
    Generic(String),
}

pub struct Parser {
    // TODO: make this private
    pub tokens: Vec<Token>,
    lexer: Lexer,
}

impl Parser {
    pub fn new() -> Parser {
        Parser {
            lexer: Lexer::new(),
            tokens: Vec::<Token>::new(),
        }
    }

    pub fn new_with_input(input: String) -> Parser {
        Parser {
            lexer: Lexer::new_with_input(input),
            tokens: Vec::<Token>::new(),
        }
    }

    pub fn parse(&mut self) -> Stmt {
        self.tokens = self.lexer.lex();
        self.tokens.pop();
        let mut tree = Stmt::Block(Vec::<Box<Stmt>>::new(), true);
        
        let mut curr_tok = self.get_next_token();

        while self.tokens.len() != 0 {
            let expr = self.parse_expression(&curr_tok.clone());
            println!("{:#?}", expr);
        }
        
        tree
    }

    pub fn print_tokens(&mut self) {
        self.lexer.lex();
        for tok in self.tokens.iter() {
            println!("{:?}", tok);
        }
    }

    // fn parse_statement(&mut self, curr_tok: &Token) -> Stmt {
    //     match curr_tok {
    //         Token::Identifier(value) => {
    //             match &value[..] {
    //                 "let" => self.parse_declare_stmt(),
    //                 _ => panic!("Unable to parse statement, current token: {:?}", &curr_tok) 
    //             }
    //         },
    //         _ => panic!("Unable to parse statement, current token: {:?}", curr_tok) 
    //     }
    // }

    // fn parse_assign_stmt(&mut self, mut curr_tok: &Token) -> Stmt {
    //     let next = self.get_next_token();
    //     match next {
            
    //     }
    // }

    // fn parse_declare_stmt(&mut self) -> Stmt {
    //     let next1 = self.look_ahead(0);
    //     let next2 = self.look_ahead(1);
    //     match (next1, next2) {

    //     }
    // }

    fn parse_expression(&mut self, curr_tok: &Token) -> Expr {
        match curr_tok {
            // Token::Identifier(value) => {
            //     let next = self.look_ahead(0);
            //     match next {
            //         Token::Identifier(value) => if value == "(".to_string() { 
            //             return self.parse_func_call(curr_tok)
            //         } else {
            //             panic!("Unexpected token while parsing expression, current token: {:?}", curr_tok)
            //         },
            //         _ => panic!("Unexpected token while parsing expression, current token: {:?}", curr_tok),
            //     }
            // },
            Token::Number(value) => {
                return self.parse_binary_exp(curr_tok);
            },
            Token::Other(value) => match value {
                '"' => {
                    Expr::Literal(self.parse_str_lit())
                },
                _ => {
                    let opcode = self.parse_opcode(curr_tok);
                    match opcode {
                        Some(OpCode::Subtraction) => {
                            let next_tok = self.get_next_token();
                            Expr::UnaryOp(opcode.unwrap(), Box::new(self.parse_expression(&next_tok)))
                        },
                        _ => panic!("Cant parse opcode."),
                    }
                } 
            },
            _ => panic!("Cant parse expression: Current token = {:?}", curr_tok),
        }
    }

    // fn parse_func_call(&mut self, curr_tok: &Token) -> Expr {
        
    // }

    fn parse_binary_exp(&mut self, curr_tok: &Token) -> Expr {
        let left: Expr;
        let opcode: OpCode;

        if self.tokens.len() == 0 { 
            return Expr::Literal(self.parse_literal(curr_tok));
        } else {
            left = Expr::Literal(self.parse_literal(curr_tok));
        }

        let mut next_tok = self.get_next_token();
        match next_tok {
            Token::Other(_) => {
                opcode = match self.parse_opcode(&next_tok) {
                    Some(op) => op,
                    None => panic!("Failed to parse opcode")
                };
            },
            _ => panic!("[ERR] Unexpected token during expression parsing: {:?}", next_tok)
        }

        next_tok = self.get_next_token();
        let right = self.parse_expression(&next_tok);

        Expr::BinaryOp(Box::new(left), opcode, Box::new(right))
    }

    fn parse_str_lit(&mut self) -> Literal {
        let mut curr_string_tok =  self.get_next_token();
        let mut lit_string = String::new();

        while curr_string_tok != Token::Other('"') {
            match curr_string_tok {
                Token::Other(value) => lit_string.push(value),
                Token::Identifier(value) => lit_string.push_str(&value[..]),
                Token::Number(value) => lit_string.push_str(&value.to_string()[..]),
                _ => panic!("Cant parse expression: Current token = {:?}", curr_string_tok),
            }
            curr_string_tok = self.get_next_token();
        }
        
        Literal::StringLit(lit_string)
    } 

    fn parse_literal(&mut self, token: &Token) -> Literal {
        match token {
            Token::Number(value) => Literal::FloatLit(*value),
            Token::Identifier(value) => self.parse_str_lit(),
            _ => panic!("Cant parse literal"),
        }
    }

    fn parse_opcode(&self, token: &Token) -> Option<OpCode> {
        match token {
            Token::Other(value) => {
                match value {
                    '+' => Some(OpCode::Addition),
                    '-' => Some(OpCode::Subtraction),
                    '*' => Some(OpCode::Multiplication),
                    '/' => Some(OpCode::Division),
                    _ => None,
                }
            },
            _ => None
        }
    }

    fn look_ahead(&self, ahead_index: usize) -> Token {
        self.tokens[ahead_index].clone()
    }

    fn get_next_token(&mut self) -> Token {
        let result = match self.tokens.clone().split_first() {
            Some((first, rest)) => {
                self.tokens = rest.to_vec();
                Ok(first.clone())
            }
            _ => Err(ParseErr::Generic(String::from("Couldn't fetch next token."))),
        };

        match result {
            Ok(next_char) => next_char,
            Err(_) => panic!("Couldnt get next token."),
        }
    }
}