# Rust Implementation of LLVM Kaleidoscope Tutorial

This is a WIP implementation of the [LLVM Kaleidoscope tutorial](http://releases.llvm.org/3.6.2/docs/tutorial/LangImpl1.html), which originally targeted C++.

## Update: 
While originally this was intended to be a true translation of the Kaleidoscope langauge detailed above,
I have probably strayed away from the original spec.



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and 
testing purposes. I give no promises of this being stable in its current form, but I hope it will eventually come 
to fruition in a full language implementation with a LLVM backend.

### Prerequisites

To get this project up and running you will require the following prerequisites.

1. Git
2. Rust.

### Installing

To install this project on your machine, run the following commands:

1. Clone the repo

```
git clone <url>
```

2. Navigate to the directory and use Rust's cargo build tool to build or run the project

```
cargo build
```

```
cargo run
```

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Acknowledgments

* LLVM, for their amazing documentation
* Rust language, for being great fun to learn :D